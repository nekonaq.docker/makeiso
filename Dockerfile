ARG BASE_IMAGE
FROM $BASE_IMAGE

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

RUN set -xe \
&& apk upgrade --update \
&& apk add --no-cache bash curl xorriso p7zip make \
;

ENV WORKDIR /data

COPY code/makeiso /usr/share/makeiso
COPY docker-entrypoint /

VOLUME $WORKDIR
WORKDIR $WORKDIR

ENTRYPOINT ["/docker-entrypoint"]
